package handlers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/mycontests/seabattle/game"
)

type create struct {
	Range int `json:"range"`
}

type ship struct {
	Coordinates string `json:"Coordinates"`
}

type shoot struct {
	Coord string `json:"coord"`
}

func errWrap(w http.ResponseWriter, err error) {
	w.WriteHeader(http.StatusBadRequest)
	w.Write([]byte(err.Error()))
}

func Create(w http.ResponseWriter, r *http.Request) {
	bodyReader := r.Body
	defer bodyReader.Close()

	body, err := ioutil.ReadAll(bodyReader)
	if err != nil {
		errWrap(w, err)
		return
	}
	param := create{}
	err = json.Unmarshal(body, &param)
	if err != nil {
		errWrap(w, err)
		return
	}

	err = game.NewGame(param.Range)
	if err != nil {
		errWrap(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}

func Clear(w http.ResponseWriter, r *http.Request) {
	game.Clear()
	w.WriteHeader(http.StatusOK)
	return
}

func GameState(w http.ResponseWriter, r *http.Request) {
	res, err := json.Marshal(game.GameState())
	if err != nil {
		errWrap(w, err)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(res)
	return
}

func AddShips(w http.ResponseWriter, r *http.Request) {
	bodyReader := r.Body
	defer bodyReader.Close()

	body, err := ioutil.ReadAll(bodyReader)
	if err != nil {
		errWrap(w, err)
		return
	}
	param := ship{}
	err = json.Unmarshal(body, &param)
	if err != nil {
		errWrap(w, err)
		return
	}

	err = game.AddShips(param.Coordinates)
	if err != nil {
		errWrap(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}

func Shoot(w http.ResponseWriter, r *http.Request) {
	bodyReader := r.Body
	defer bodyReader.Close()

	body, err := ioutil.ReadAll(bodyReader)
	if err != nil {
		errWrap(w, err)
		return
	}
	param := shoot{}
	err = json.Unmarshal(body, &param)
	if err != nil {
		errWrap(w, err)
		return
	}

	details, err := game.Shoot(param.Coord)
	if err != nil {
		errWrap(w, err)
		return
	}

	res, err := json.Marshal(details)

	w.WriteHeader(http.StatusOK)
	w.Write(res)
	return
}
