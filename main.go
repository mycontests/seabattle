package main

import (
	"net/http"

	"gitlab.com/mycontests/seabattle/handlers"

	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/create-matrix", handlers.Create).Methods("POST")
	r.HandleFunc("/clear", handlers.Clear).Methods("POST")
	r.HandleFunc("/ship", handlers.AddShips).Methods("POST")
	r.HandleFunc("/shot", handlers.Shoot).Methods("POST")
	r.HandleFunc("/state", handlers.GameState).Methods("GET")

	http.ListenAndServe(":80", r)
}
