package game

import (
	"errors"
	"fmt"
	"sort"
	"strconv"
	"strings"
)

var _game game
var _shots int

type ship struct {
	cells     []*cell
	destroyed bool
	knocked   bool
}

type cell struct {
	letter  rune
	number  int
	shot    bool
	knocked bool
	ship    *ship
}

type game struct {
	pitch map[rune]map[int]*cell
	ships []*ship
}

type shotResult struct {
	Destroy bool `json:"destroy"`
	Knock   bool `json:"knock"`
	End     bool `json:"end"`
}

type stateResult struct {
	ShipCount int `json:"ship_count"`
	Destroyed int `json:"destroyed"`
	Knocked   int `json:"knocked"`
	ShotCount int `json:"shotCount"`
}

func Clear() {
	_game.pitch = nil
	_game.ships = nil
	_shots = 0
}

func NewGame(size int) error {
	if _game.pitch != nil {
		return errors.New("game has already been created")
	}
	_game.pitch = map[rune]map[int]*cell{}
	for i := 0; i < 26 && i < size; i++ {
		col := rune('A' + i)
		_game.pitch[col] = map[int]*cell{}
		for i := 1; i <= 26 && i <= size; i++ {
			_game.pitch[col][i] = &cell{
				letter: col,
				number: i,
			}
		}
	}

	return nil
}

func AddShips(shipsCoords string) error {
	if len(_game.ships) != 0 && !areAllDestroyed() {
		return errors.New("ships have already been installed")
	}
	if len(_game.ships) != 0 {
		n := len(_game.pitch)
		Clear()
		NewGame(n)
	}
	ships := strings.Split(shipsCoords, ",")
	for _, sh := range ships {
		err := addShip(sh)
		if err != nil {
			return err
		}
	}
	return nil
}

func addShip(shipCoords string) (err error) {
	borderCoords := strings.Split(shipCoords, " ")

	letterFrom, numberFrom, err := parseCoord(borderCoords[0])
	if err != nil {
		return
	}

	letterTo, numberTo, err := parseCoord(borderCoords[1])
	if err != nil {
		return
	}

	if _, ok := _game.pitch[letterFrom][numberFrom]; !ok {
		return errors.New("ship's coords are outside the pitch")
	}
	if _, ok := _game.pitch[letterFrom][numberTo]; !ok {
		return errors.New("ship's coords are outside the pitch")
	}
	if _, ok := _game.pitch[letterTo][numberFrom]; !ok {
		return errors.New("ship's coords are outside the pitch")
	}
	if _, ok := _game.pitch[letterTo][numberTo]; !ok {
		return errors.New("ship's coords are outside the pitch")
	}

	ship := &ship{}
	for i := letterFrom; i <= letterTo; i++ {
		for j := numberFrom; j <= numberTo; j++ {
			if _game.pitch[i][j].ship == nil {
				cell := &cell{
					letter: i,
					number: j,
					ship:   ship,
				}
				ship.cells = append(ship.cells, cell)
				_game.pitch[i][j] = cell
			} else {
				return errors.New("ship cannot override another ship")
			}
		}
	}

	_game.ships = append(_game.ships, ship)
	return nil
}

func Shoot(coords string) (*shotResult, error) {
	letter, number, err := parseCoord(coords)
	if err != nil {
		return nil, err
	}
	if _game.pitch[letter][number].shot {
		return nil, errors.New("cell has already been shot")
	}
	_game.pitch[letter][number].shot = true

	destroy := false
	knocked := false
	if _game.pitch[letter][number].ship != nil {
		_game.pitch[letter][number].knocked = true
		hitShip(_game.pitch[letter][number].ship)
		knocked = true
		destroy = _game.pitch[letter][number].ship.destroyed
	}

	_shots++
	return &shotResult{
		Destroy: destroy,
		Knock:   knocked,
		End:     areAllDestroyed(),
	}, nil
}

func hitShip(s *ship) {
	destroyed := true
	for _, c := range s.cells {
		if !c.knocked {
			destroyed = false
		}
	}
	s.destroyed = destroyed
	s.knocked = true
}

func GameState() stateResult {
	return stateResult{
		ShipCount: len(_game.ships),
		Destroyed: getDestroyedCount(),
		Knocked:   getKnockedCount(),
		ShotCount: _shots,
	}
}

func getDestroyedCount() (destroyed int) {
	for _, v := range _game.ships {
		if v.destroyed {
			destroyed++
		}
	}
	return
}

func getKnockedCount() (knocked int) {
	for _, v := range _game.ships {
		if v.knocked {
			knocked++
		}
	}
	return
}

func areAllDestroyed() bool {
	all := true
	for _, v := range _game.ships {
		if !v.destroyed {
			all = false
			break
		}
	}
	return all
}

func parseCoord(coord string) (letter rune, number int, err error) {
	if len(coord) == 2 {
		letter = rune(coord[1])
		number, err = strconv.Atoi(coord[0:1])
	} else if len(coord) == 3 {
		letter = rune(coord[2])
		number, err = strconv.Atoi(coord[0:2])
	} else {
		err = errors.New("wrong coord format")
	}
	if err != nil {
		return
	}

	if _, ok := _game.pitch[letter]; !ok {
		err = errors.New("coords are outside the pitch")
		return
	}

	if _, ok := _game.pitch[letter][number]; !ok {
		err = errors.New("coords are outside the pitch")
		return
	}

	return
}

func PrintGame() {
	letters := make([]int, len(_game.pitch))
	numbers := make([]int, len(_game.pitch))

	number := 1
	for letter := range _game.pitch {
		letters[number-1] = int(letter)
		numbers[number-1] = number
		number++
	}
	sort.Ints(letters)
	sort.Ints(numbers)

	for _, let := range letters {
		for _, num := range numbers {
			c := _game.pitch[rune(let)][num]
			fmt.Printf("%s %d %t %t %p\t||", string(c.letter), c.number, c.shot, c.knocked, c.ship)
		}
		fmt.Println()
	}
}
